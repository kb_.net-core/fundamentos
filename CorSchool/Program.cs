﻿using System;
using CorSchool.app;
using CorSchool.entity;
using static System.Console;

namespace CorSchool
{
    class Program
    {
        static void Main(string[] args)
        {
            var engine = new SchoolEngine();
            engine.inicializar();
            
            
            WriteLine("----------PRINTERS COLLECTIONS----------");
            PrinterCoursesSchool(engine.school);
            

            
        }

        private static void PrinterCoursesSchool(School school)
        {
            //if(school != null && school.courses != null)       ===     if(school?.courses != null)
            if(school?.courses != null)
            {
                foreach (var item in school.courses)
                {
                    WriteLine($"Nombre: {item.name}, TipoJornada: {item.typeWork}, Id: {item.uniqueId}");
                }
            }
                                   
        }


    }
}

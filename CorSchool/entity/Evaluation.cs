using System;

namespace CorSchool.entity
{
    public class Evaluation
    {
        public string uniqueId { get; private set; }
        public string name { get; set; }

        public Student student { get; set; }
        public Subject subject { get; set; }

        public float note { get; set; }

        public Evaluation() => uniqueId = Guid.NewGuid().ToString();
    }
}
using System;
using System.Collections.Generic;

namespace CorSchool.entity
{
    public class School
    {
        public string UniqueId { get; private set; } = Guid.NewGuid().ToString();
        public string name { get; set; }
        public int yearCreation { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public TypeSchool typeSchool { get; set; }
        public List<Course> courses { get; set; }
        

        //Constructor  ---> 1~forma
        /*
        public School (string name, int year)
        {
            this.name = name;
            this.yearCreation = year;
        }
        */

        //Constructor  ---> 2-forma
        public School (string names, int year) => (name, yearCreation) = (names, year);

        //Constructor con parametros opcionales ---> 3-forma
        public School (string names, 
                       int year, 
                       TypeSchool typeSchool,
                       string country ="", 
                       string city="")
        {
            (name, yearCreation) = (names, year);
            this.country = country;
            this.city = city;
        }
        
        //ToString             
        public override string ToString()
        {
            //return $"Nombre: {name}, FechaCreacion: {yearCreation}, TipoEscuela: {typeSchool}, Pais: {country}, Ciudad:{city}";
            return $"Nombre: \"{name}\", FechaCreacion: \"{yearCreation}\", TipoEscuela: \"{typeSchool}\", Pais: \"{country}\", Ciudad:\"{city}\"";
        }   
           
    }

}
using System;

namespace CorSchool.entity
{
    public class Student
    {
        public string uniqueId { get; private set; }
        public string name { get; set; }

        public Student() => uniqueId = Guid.NewGuid().ToString();
    }
}
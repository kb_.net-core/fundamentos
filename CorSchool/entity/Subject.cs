using System;

namespace CorSchool.entity
{
    public class Subject
    {
        public string uniqueId { get; private set; }
        public string name { get; set; }

        public Subject() => uniqueId = Guid.NewGuid().ToString();
    }
}
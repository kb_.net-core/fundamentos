using System;
using System.Collections.Generic;

namespace CorSchool.entity
{
    public class Course
    {
        public string uniqueId { get; private set; }
        public string name { get; set; }
        public string work { get; set; }
        public TypeWork typeWork { get; set; }
        public List<Subject> subjects { get; set; }
        public List<Student> students { get; set; }


        //Generamos el codigo aleatoreamente, a traves del Constructor
        public Course() => uniqueId = Guid.NewGuid().ToString();
        

    }
}
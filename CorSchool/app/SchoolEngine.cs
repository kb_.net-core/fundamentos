using System;
using System.Collections.Generic;
using System.Linq;
using CorSchool.entity;

namespace CorSchool.app
{
    public class SchoolEngine
    {
        public School school { get; set; }

        public SchoolEngine()
        {
            
        }

        public void inicializar()
        {
            school = new School("PLATZI S.A.S.", 2020, TypeSchool.primaria, city: "Colombia"); 
            
            cargarCursos();   
            cargarAsignaturas();    
            cargarEvaluaciones();      
        }

        private void cargarEvaluaciones()
        {
            throw new NotImplementedException();
        }

        private void cargarAsignaturas()
        {
            foreach (var course in school.courses)
            {
                var listSubject = new List<Subject>()
                {
                    new Subject{ name="Matematicas"},
                    new Subject{ name="Educacion fisica"},
                    new Subject{ name="Castellano"},
                    new Subject{ name="CIencias Naturales"},
                    new Subject{ name="Filosofia"}
                };
                course.subjects = listSubject;
            }
        }

        private List<Student> generarNombreAlAzar(int cant)
        {
            string[] nombre1 = { "Alba", "Felipa", "Eusebio", "Farid", "Donald", "Alvaro", "Nicolás" };
            string[] apellido1 = { "Ruiz", "Sarmiento", "Uribe", "Maduro", "Trump", "Toledo", "Herrera" };
            string[] nombre2 = { "Freddy", "Anabel", "Rick", "Murty", "Silvana", "Diomedes", "Nicomedes", "Teodoro" };

            //todo contra todos.
            var listStudent = from n1 in nombre1
                              from n2 in nombre2
                              from a1 in apellido1
                              select new Student{ name = $"{n1} {n2} {a1}" };
            
            return listStudent.OrderBy( (al) => al.uniqueId).Take(cant).ToList();

        }

        private void cargarCursos()
        {
            school.courses = new List<Course>(){
                new Course() {name = "101", typeWork = TypeWork.morning },
                new Course() {name = "201", typeWork = TypeWork.morning },
                new Course() {name = "301", typeWork = TypeWork.late },
                new Course() {name = "401", typeWork = TypeWork.night },
                new Course() {name = "501", typeWork = TypeWork.night },
                new Course() {name = "601", typeWork = TypeWork.night}
            };
            Random rnd = new Random();
            
            foreach (var c in school.courses)
            {
                int cantRandom = rnd.Next(5,20);
                c.students = generarNombreAlAzar(cantRandom);
            }  
        }
    }
}
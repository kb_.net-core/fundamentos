﻿using System;

namespace CoreEscuela
{
    class Escuela
    {
        public string name;
        public string direction;
        public int yearFundation;
        public string ceo = "Freddy Vega";

        public void timbrar(){
            //Todo
            Console.Beep(2000,3000);
        } 

    }

    class Program
    {
        static void Main(string[] args)
        {
            
            var miEscuerla = new Escuela();
            miEscuerla.name="ELvis HUacachi Caldas";
            miEscuerla.direction="Urb. Pachacamac";
            miEscuerla.yearFundation=2020;
            
            Console.WriteLine("TIMBRAR");
            miEscuerla.timbrar();

        }
    }
}

# FUNDAMENTOS DE C# CON .NET CORE

    C# es creado por Anders Hejilsberg y su equipo
    C# es un lenguaje multiparadigma

    Es un lenguaje strong typing(fuertemente tipado)
        -Imperativo
        -Declarativo
        -alineado con el paradigma Functional
        -orientado a objetos(basado en clases)
        -orientado a componentes

## Creacion de protecto
    `dotnet new <shortName>`     --> console, classlib, wpf,...

    Cracion de un proyecto en una carpeta especifica
    `dotnet new console --output platziHelloWorld`

## Ejecucion    
    `dotnet run` 

    Ejecuta el archivo Program.cs

## Compilar el proyecto
    `dotnet build`

    Compilar para produccion, con un sistema operativo en especifico:
    - win7-x64
    - wind10-x64
    - osx.10.11-x64
    - ubuntu.16.04-x64

    `dotnet build -c Release -r win10-x64`

    Para ejecutar desde la carpeta bin (compilados) 
    `dotnet FUNDAMENTOS.dll`

### Proyecto CorEscuela
    OBJETIVOS
    
    Administrar una escuela pequeña.
    Manejar los alumnos de cada grado.
    Controlar sus asignaturas.
    Controlar sus evaluaciones.
    Elaborar informes.


## ShortCut
    `prop`          --> Para crear las entidades con get y set
    `propfull`      --> Para crear las entidad explicitamente
    `cw`            --> Para impresion

    `rename Symbol` --> Cambiar de nombre BAse y automaticamente se actualiza todas sus referencias.
    
## Extensiones para VSCode
    `C#`
    `C# Extensions`

## Operadores
    `||`    -->   o
    `&&`    -->   y



